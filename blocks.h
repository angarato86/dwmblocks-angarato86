//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/ /*Command*/	 	                            /*Update Interval*/	/*Update Signal*/
    {" 🐧 ", "$HOME/.config/dwm/kernel",	 					360,		          2},

	/*{" Upt: ", "$HOME/.config/dwm/upt",		 						60,		          2},*/

	{" 📦 ", "$HOME/.config/dwm/pacupdate",  					360,		          9},
	
	{" 🖥️ ", "$HOME/.config/dwm/memory",	 						6,		              1},

	/*{" Vol: ", "$HOME/.config/dwm/volume",     						0,		              10},*/

	{" 🗓️ ", "$HOME/.config/dwm/clock",	    					60,	              0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
