Dwmblocks is a modular status bar for dwm written in c.  This is my personal build of dwmblocks.

Modifying dwmblocks
The statusbar is made from text output from scripts found in my .config/dwm. Blocks are added and removed by editing the blocks.h header file.

To use the same scripts I use, just copy them from the scripts folder to .config/dwm
